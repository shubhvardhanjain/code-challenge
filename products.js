const axios = require("axios")

const configs = {
    productSource1: {
        url: "http://localhost:1234",
        authToken: "abc9456"
    },
    productSource2: {
        url: "http://localhost:4567",
        username: "abc",
        password: "1234"
    },
    dest: {
        url: "http://localhost:6789",
        token: "abc123",
        value: "4321"
    }
    // configs for stock source can be added here
}


const generateRequest = (source, options) => {
    let sources = {
        "productSource1":  {
            method: 'GET',
            url: `${configs.productSource1.url}/products${options.next50?"":"?next50=true"}`,
            headers: { 'Authentication': `Bearer ${config.productSource1.authToken}` }
        },
        "productSource2":  {
            method: 'GET',
            url: `${configs.productSource1.url}/products${options.next50?"":"?next50=true"}`,
            auth:{username:configs.productSource2.username,password:configs.productSource2.password}
        }
        // requests for stock data can also be added here
    }
    if(sources[source]){
        return sources[source] 
    }else {
        throw new Error("Invalid source")
    }
}


const downloadRequest = async (source,options={next50:false}) => {
    try {
        let data = await axios(generateRequest(source,options))
        return data.products
    } catch (error) {
        console.log(error)
        throw error
    }
}

const refineItem = async (sourceType,itemData) => {
    let generateSKU = (str)=>{return str.replace(/ /g,"_")}
    // data : data for individual product 
    let formatters = {
        "productSource1":(data)=>{
            const parSku = generateSKU(data.name)
            let product = {
                parent:{
                    sku:parSku,
                    name:data.name,
                    description:data.description,
                    linkpoints:[]
                },
                children:[]
            }
            data.childrenLinkPoint.map(clp=>{
                let childSku = generateSKU(clp.name)
                product.parent.linkpoints.push({
                    name:clp.name,
                    child_skus:clp.values
                })
                let cData = data.children.find(itm=>{return itm["name"]==clp["name"]})
                product.children.push({
                    sku:childSku,
                    value:clp.value,
                    description:cData.description,
                    link:{
                        parentSku:parentSku,
                        value:cData.parentLinkPoint.value,
                        name:cData.parentLinkPoint.name
                    }
                })
            })

            return product
        },
        "productSource2":(data)=>{
            const parSku = generateSKU(data.standardfields.name)
            let product = {
                parent:{
                    sku:parSku,
                    name:data.standardfields.name,
                    description:data.standardfields.description,
                    linkpoints:[]
                },
                children:[]
            }
            // .... 
            return product
        }
        // more formatter can be added here 
    }

    if(formatters[sourceType]){
        return formatters[sourceType](itemData)
    }else{
        throw new Error("Invalid source")
    }
} 


const saveItemToDataBase = async(sourceType,original,refined) =>{
    let product = JSON.parse(JSON.stringify(refined))
    product["original"] = original
    product["addedOn"] = new Date()
    product["source"] = sourceType
    // save this json in the database accoring to the type of item 
}

const uploadItem = async(productData)=>{
    // this method can be modifed to upload both types of entities products and stock
    let header ={}
    header[configs.dest.token] = configs.dest.value
    await axios({
        method:"POST",
        url:configs.dest.url+"/products",
        headers:header,
        data:productData
    })
}